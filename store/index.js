import moment from 'moment'
import SpotifyWebApi from 'spotify-web-api-js'
import createPersistedState from 'vuex-persistedstate'

import { createWorker, PSM, OEM } from 'tesseract.js'

var spotifyApi = new SpotifyWebApi()
export const plugins = [createPersistedState()]
function initialState() {
  return {
    loggedIn: false,
    currentUser: null,
    searchText: null,
    spotifyClientAccessToken: null,
    tokenValidUntil: null,
    searchingTracks: false,
    songs: [],
    selectedSongIds: [],
    playlists: [],
    previewedTrack: null,
    saving: false,
    rightDrawer: false,
    drawerTrack: null,
    snackMessage: '',
    snackColor: 'info',
    showSnack: false,
    ocrProgress: 0,
    ocrWorking: false
  }
}

export const state = () => {
  return initialState()
}

function debounce(callback, wait, immediate) {
  var timeout
  return function() {
    var context = this,
      args = arguments
    var later = function() {
      timeout = null
      if (!immediate) callback.apply(context, args)
    }
    var callNow = immediate && !timeout
    clearTimeout(timeout)
    timeout = setTimeout(later, wait)
    if (callNow) callback.apply(context, args)
  }
}

const debouncedSetSearchText = debounce(function(commit, text) {
  commit('setSearchText', { text })
}, 1000)

export const mutations = {
  setSearchText(state, payload) {
    state.searchText = payload.text
  },
  setCurrentUser(state, user) {
    state.currentUser = user
  },
  setClientAccessToken(state, { accessToken }) {
    state.spotifyClientAccessToken = accessToken
    state.tokenValidUntil = moment().add(1, 'h')
    state.loggedIn = true
  },
  addTrackResult(state, payload) {
    const found = payload.found
    const { selectedTrackIndex } = payload
    state.songs.push({
      searched: payload.search,
      found,
      selectedTrackIndex,
      tracks: payload.tracks
    })
  },
  setSearchingTracks(state, isSearching) {
    state.searchingTracks = isSearching
  },
  resetTracks: function(state) {
    // state.tracks = []
    state.songs = []
  },
  removeTrack: function(state, trackIndex) {
    state.songs.splice(trackIndex, 1)
    // state.tracks.splice(trackIndex, 1)
    // state.searchTracks.splice(trackIndex, 1)
  },
  replaceSong: function(state, { song, songIndex }) {
    const songs = [...state.songs]
    songs[songIndex] = song
    state.songs = songs
  },
  replaceTrack: function(state, { trackIndex, newSelectedTrackIndex }) {
    const songs = [...state.songs]
    songs[trackIndex].selectedTrackIndex = newSelectedTrackIndex
    state.songs = songs
  },
  resetState: function(state) {
    const initial = initialState()
    Object.keys(initial).forEach(key => {
      state[key] = initial[key]
    })
  },
  SET_PREVIEW_TRACK: function(state, track) {
    state.previewedTrack = track
  },
  SAVING_TRACK: function(state) {
    state.saving = true
  },
  TRACK_SAVED: function(state) {
    state.saving = false
  },
  OPEN_DRAWER: function(state) {
    state.rightDrawer = true
  },
  CLOSE_DRAWER: function(state) {
    state.rightDrawer = false
  },
  SET_DRAWER: function(state, val) {
    state.rightDrawer = val
  },
  SET_DRAWER_TRACK: function(state, { track, trackIndex }) {
    state.drawerTrack = { track: track, trackIndex }
  },
  setSelectedSongIds: function(state, songIds) {
    state.selectedSongIds = songIds
  },
  setUserPlaylists: function(state, playlists) {
    state.playlists = playlists.items
      .filter(pl => pl.owner.id === state.currentUser.id)
      .map(pl => ({
        id: pl.id,
        image: pl.images && pl.images.length ? pl.images[0].url : '',
        name: pl.name,
        tracksAmount: pl.tracks.total,
        public: pl.public,
        loading: false
      }))
  },
  setPlaylistLoading: function(state, { playlistId, loading }) {
    const playlists = [...state.playlists]
    const plIndex = playlists.findIndex(pl => pl.id === playlistId)
    playlists[plIndex].loading = loading
    state.playlists = playlists
  },
  SET_SEARCH_IMAGE: function(state, img) {
    state.searchImage = img
  },
  showSnack: function(state, { msg, color }) {
    state.snackMessage = msg
    state.snackColor = color
    state.showSnack = true
  },
  hideSnack: function(state) {
    state.showSnack = false
  },
  SET_OCR_PROGRESS: function(state, p) {
    state.ocrProgress = p
  },
  SET_OCR_WORKING: function(state, val) {
    state.ocrWorking = val
  }
}
export const actions = {
  changeSearchText({ commit }, text) {
    debouncedSetSearchText(commit, text)
  },
  changeSearchImage({ commit }, img) {
    commit('SET_SEARCH_IMAGE', img)
  },
  spotifyLogin() {
    var client_id = 'e004632886b24724b8502b8cbeecb689'
    var redirect_uri = `${process.env.baseUrl}/auth`
    var scope =
      'user-read-private user-read-email user-library-modify user-library-read'
    var url = 'https://accounts.spotify.com/authorize'
    url += '?response_type=token'
    url += '&client_id=' + encodeURIComponent(client_id)
    url += '&scope=' + encodeURIComponent(scope)
    url += '&redirect_uri=' + encodeURIComponent(redirect_uri)
    //url += '&state=' + encodeURIComponent(state);

    // TODO: open login window in new small browser window instead of redirect
    window.location = url
  },
  async getUserInfo({ commit, state }) {
    spotifyApi.setAccessToken(state.spotifyClientAccessToken)
    try {
      const currentUser = await spotifyApi.getMe()
      commit('setCurrentUser', currentUser)
    } catch (e) {
      commit('showSnack', {
        msg: 'An error occurred',
        color: 'error'
      })
    }
  },
  async getTracksFromApi({ dispatch, commit, state }) {
    // if user is logged in search tracks from text tracks array
    if (
      state.spotifyClientAccessToken &&
      moment().isBefore(state.tokenValidUntil)
    ) {
      const tracks = state.searchText.trim().split('\n')

      spotifyApi.setAccessToken(state.spotifyClientAccessToken)
      commit('resetTracks')
      try {
        const searchResults = await Promise.all(
          tracks.map(async searchTrack => {
            const trackResult = await spotifyApi.searchTracks(searchTrack)
            const resultsFound = trackResult.tracks.total > 0
            const trackItems = trackResult.tracks.items
            return {
              search: searchTrack,
              tracks: trackItems,
              found: resultsFound,
              selectedTrackIndex: 0
            }
          })
        )
        searchResults.forEach(tr => commit('addTrackResult', tr))
        commit('setSearchingTracks', false)
      } catch (err) {
        commit('showSnack', {
          msg: 'An error occurred',
          color: 'error'
        })
      }
    } else {
      commit('setSearchingTracks', true)
      dispatch('spotifyLogin')
    }
  },
  async getUserPlaylists({ dispatch, commit, state }) {
    // TODO: if user is logged in search tracks from text tracks array
    if (
      state.spotifyClientAccessToken &&
      moment().isBefore(state.tokenValidUntil)
    ) {
      spotifyApi.setAccessToken(state.spotifyClientAccessToken)
      const playlists = await spotifyApi.getUserPlaylists()
      commit('setUserPlaylists', playlists)
    } else {
      //commit('setSearchingTracks', true)
      dispatch('spotifyLogin')
    }
  },
  async changeSearchedSong({ commit, state }, { searchQuery, songIndex }) {
    commit('setSearchingTracks', true)
    // commit('removeTrack', songIndex)
    spotifyApi.setAccessToken(state.spotifyClientAccessToken)
    try {
      const searchResults = await spotifyApi.searchTracks(searchQuery)
      const resultsFound = searchResults.tracks.total > 0
      const trackItems = searchResults.tracks.items
      commit('replaceSong', {
        song: {
          searched: searchQuery,
          tracks: trackItems,
          found: resultsFound,
          selectedTrackIndex: 0
        },
        songIndex
      })
      commit('setSearchingTracks', false)
    } catch (e) {
      commit('showSnack', {
        msg: 'An error occurred',
        color: 'error'
      })
    }
  },
  async previewTrack({ commit, state }, songIndex) {
    const song = state.songs[songIndex]

    if (song.tracks[song.selectedTrackIndex].preview_url) {
      commit('SET_PREVIEW_TRACK', song.tracks[song.selectedTrackIndex])
    } else {
      spotifyApi.setAccessToken(state.spotifyClientAccessToken)
      try {
        const track = await spotifyApi.getTrack(
          song.tracks[song.selectedTrackIndex].id
        )
        commit('SET_PREVIEW_TRACK', track)
      } catch (e) {
        commit('showSnack', {
          msg: 'An error occurred',
          color: 'error'
        })
      }
    }
  },
  resetPreviewTrack({ commit }) {
    commit('SET_PREVIEW_TRACK', null)
  },
  saveTrack({ commit, state }, trackId) {
    spotifyApi.setAccessToken(state.spotifyClientAccessToken)
    commit('SAVING_TRACK')
    spotifyApi.addToMySavedTracks([trackId]).then(
      data => {
        commit('TRACK_SAVED', data)
      },
      err => {
        commit('ERROR')
        commit('showSnack', {
          msg: 'An error occurred',
          color: 'error'
        })
      }
    )
  },
  resetState({ commit }) {
    commit('resetState')
  },
  toggleDrawer({ commit, state }) {
    if (state.rightDrawer) {
      commit('CLOSE_DRAWER')
    }
  },
  openDrawer({ commit, state }, itemIndex) {
    commit('SET_DRAWER_TRACK', {
      track: state.songs[itemIndex],
      trackIndex: itemIndex
    })
    commit('OPEN_DRAWER')
  },
  replaceTrack({ commit }, { trackIndex, newSelectedTrackIndex }) {
    commit('replaceTrack', {
      trackIndex,
      newSelectedTrackIndex
    })
    commit('CLOSE_DRAWER')
  },
  selectSongs({ commit }, songIds) {
    commit('setSelectedSongIds', songIds)
  },
  async addToPlaylist({ commit, state }, playlistId) {
    if (state.selectedSongIds.length) {
      const songIndexes = state.selectedSongIds.map(song => song._id)
      const tracksToAdd = []
      for (const i of songIndexes) {
        if (state.songs[i].tracks[state.songs[i].selectedTrackIndex]) {
          tracksToAdd.push(
            state.songs[i].tracks[state.songs[i].selectedTrackIndex].uri
          )
        }
      }
      commit('setPlaylistLoading', { playlistId, loading: true })
      try {
        spotifyApi.setAccessToken(state.spotifyClientAccessToken)
        const added = await spotifyApi.addTracksToPlaylist(
          playlistId,
          tracksToAdd
        )
        commit('setPlaylistLoading', { playlistId, loading: false })
        commit('showSnack', {
          msg: 'Added songs to Playlist',
          color: 'success'
        })
      } catch (e) {
        commit('setPlaylistLoading', { playlistId, loading: false })
        commit('showSnack', {
          msg: 'An error occurred',
          color: 'error'
        })
      }
    }
  },
  async imageToText({ commit, state }, img) {
    const worker = createWorker({
      logger: m => {
        if (m.status === 'recognizing text') {
          commit('SET_OCR_PROGRESS', m.progress)
        }
      }
    })

    await worker.load()
    await worker.loadLanguage('eng')
    await worker.initialize('eng', OEM.LSTM_ONLY)
    await worker.setParameters({
      tessedit_pageseg_mode: PSM.SINGLE_BLOCK
    })
    commit('SET_OCR_WORKING', true)
    const {
      data: { text }
    } = await worker.recognize(img)

    const regex = /\n\n/gm

    commit('setSearchText', { text: text.replace(regex, '\n') })

    commit('SET_OCR_WORKING', false)
  }
}
