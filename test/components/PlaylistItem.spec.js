import PlaylistItem from '@/components/PlaylistItem'
import { createLocalVue, mount } from '@vue/test-utils'
import faker from 'faker'
import Vue from 'vue'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

Vue.use(Vuetify)
const localVue = createLocalVue()
localVue.use(Vuex)

faker.seed(123123)

describe('PlaylistItem', function() {
  let vuetify
  let actions
  let store

  beforeEach(() => {
    vuetify = new Vuetify()
  })

  test('renders without error', () => {
    faker.seed(123123)
    const wrapper = mount(PlaylistItem, {
      vuetify,
      localVue,
      propsData: {
        pl: {
          name: faker.name.findName(),
          id: faker.random.uuid(),
          image: faker.image.imageUrl(),
          public: faker.random.boolean(),
          tracksAmount: faker.random.number()
        },
        index: 0
      }
    })
    expect(wrapper.element).toMatchSnapshot()
  })

  test('calls addToPlaylist when button is clicked', () => {
    faker.seed(123123)
    actions = {
      addToPlaylist: jest.fn()
    }
    store = new Vuex.Store({
      actions
    })
    const wrapper = mount(PlaylistItem, {
      vuetify,
      localVue,
      store,
      propsData: {
        pl: {
          name: faker.name.findName(),
          id: faker.random.uuid(),
          image: faker.image.imageUrl(),
          public: faker.random.boolean(),
          tracksAmount: faker.random.number()
        },
        index: 0
      }
    })
    wrapper.find('.add-btn').trigger('click')
    expect(actions.addToPlaylist).toHaveBeenCalled()
  })
})
