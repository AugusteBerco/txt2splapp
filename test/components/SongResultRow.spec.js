import SongResultRow from '@/components/SongResultRow'
import { createLocalVue, mount } from '@vue/test-utils'
import faker from 'faker'
import Vue from 'vue'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

faker.seed(123123123)

Vue.use(Vuetify)
const localVue = createLocalVue()
localVue.use(Vuex)

describe('SongResultRow', () => {
  let vuetify
  beforeEach(() => {
    vuetify = new Vuetify()
  })

  test('snapshot test', () => {
    faker.seed(123123123)
    const wrapper = mount(SongResultRow, {
      vuetify,
      propsData: {
        item: {
          name: faker.name.firstName(),
          found: true,
          searched: faker.lorem.words(4),
          artist: faker.name.findName(),
          album: faker.name.findName()
        },
        index: 0,
        isSelected: false,
        previewTrack: jest.fn(),
        saveTrack: jest.fn(),
        changeSearchedRow: jest.fn(),
        openDrawer: jest.fn(),
        select: jest.fn()
      }
    })
    expect(wrapper.element).toMatchSnapshot()
  })
})
