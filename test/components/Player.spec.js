import Player from '@/components/Player'
import { createLocalVue, mount } from '@vue/test-utils'
import Vue from 'vue'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

Vue.use(Vuetify)
const localVue = createLocalVue()
localVue.use(Vuex)

describe('Player', function() {
  let state
  let actions
  let store
  let vuetify

  beforeEach(() => {
    vuetify = new Vuetify()
  })

  test('renders without error', () => {
    state = {
      previewedTrack: {
        name: 'test',
        artists: [{ name: 'test' }],
        album: { name: 'test', images: [{ url: 'test' }, { url: 'test' }] }
      }
    }
    store = new Vuex.Store({
      state
    })
    const wrapper = mount(Player, { vuetify, store, localVue })
    expect(wrapper.html()).toContain('test')
    expect(wrapper.element).toMatchSnapshot()
  })

  test('resetPreviewTrack is called when closed', () => {
    state = {
      previewedTrack: {
        name: 'test',
        artists: [{ name: 'test' }],
        album: { name: 'test', images: [{ url: 'test' }, { url: 'test' }] }
      }
    }
    actions = {
      resetPreviewTrack: jest.fn()
    }
    store = new Vuex.Store({
      state,
      actions
    })
    const wrapper = mount(Player, { vuetify, store, localVue })
    wrapper.find('#close-player-btn').trigger('click')
    expect(actions.resetPreviewTrack).toHaveBeenCalled()
  })
})
