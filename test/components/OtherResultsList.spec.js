import OtherResultsList from '@/components/OtherResultsList'
import { createLocalVue, mount } from '@vue/test-utils'
import Vue from 'vue'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

Vue.use(Vuetify)
const localVue = createLocalVue()
localVue.use(Vuex)

describe('OtherResultsList', () => {
  let state
  let actions
  let store
  let vuetify
  beforeEach(() => {
    vuetify = new Vuetify()
  })

  test('snapshot test', () => {
    state = {
      drawerTrack: {
        track: {
          tracks: [
            {
              name: 'test1',
              artists: [{ name: 'test1' }],
              album: { name: 'test1', images: [{}, {}, { url: 'test1' }] }
            },
            {
              name: 'test2',
              artists: [{ name: 'test2' }],
              album: { name: 'test2', images: [{}, {}, { url: 'test2' }] }
            }
          ],
          selectedTrackIndex: 0
        },
        trackIndex: 0
      }
    }
    actions = {
      replaceTrack: jest.fn()
    }

    store = new Vuex.Store({
      state,
      actions
    })
    const wrapper = mount(OtherResultsList, { vuetify, store, localVue })
    expect(wrapper.element).toMatchSnapshot()
  })

  test('does not render if no other tracks', () => {
    state = {
      drawerTrack: {
        track: {
          tracks: [],
          selectedTrackIndex: 0
        },
        trackIndex: 0
      }
    }
    actions = {
      replaceTrack: jest.fn()
    }

    store = new Vuex.Store({
      state,
      actions
    })
    const wrapper = mount(OtherResultsList, { vuetify, store, localVue })
    expect(wrapper.html()).toEqual('<div></div>')
  })

  test('dispatches replaceTrack when replace button is clicked', () => {
    state = {
      drawerTrack: {
        track: {
          tracks: [
            {
              name: 'test1',
              artists: [{ name: 'test1' }],
              album: { name: 'test1', images: [{}, {}, { url: 'test1' }] }
            },
            {
              name: 'test2',
              artists: [{ name: 'test2' }],
              album: { name: 'test2', images: [{}, {}, { url: 'test2' }] }
            }
          ],
          selectedTrackIndex: 0
        },
        trackIndex: 0
      }
    }
    actions = {
      replaceTrack: jest.fn()
    }

    store = new Vuex.Store({
      state,
      actions
    })
    const wrapper = mount(OtherResultsList, { vuetify, store, localVue })
    wrapper.find('.v-btn').trigger('click')
    expect(actions.replaceTrack).toHaveBeenCalled()
  })

  test('is empty if only one track result', () => {
    state = {
      drawerTrack: {
        track: {
          tracks: [
            {
              name: 'test1',
              artists: [{ name: 'test1' }],
              album: { name: 'test1', images: [{}, {}, { url: 'test1' }] }
            }
          ],
          selectedTrackIndex: 0
        },
        trackIndex: 0
      }
    }
    actions = {
      replaceTrack: jest.fn()
    }

    store = new Vuex.Store({
      state,
      actions
    })
    const wrapper = mount(OtherResultsList, { vuetify, store, localVue })

    expect(wrapper.html()).toEqual('<div></div>')
  })
  test('has 1 v-cards for 2 items', () => {
    state = {
      drawerTrack: {
        track: {
          tracks: [
            {
              name: 'test1',
              artists: [{ name: 'test1' }],
              album: { name: 'test1', images: [{}, {}, { url: 'test1' }] }
            },
            {
              name: 'test2',
              artists: [{ name: 'test2' }],
              album: { name: 'test2', images: [{}, {}, { url: 'test2' }] }
            }
          ],
          selectedTrackIndex: 0
        },
        trackIndex: 0
      }
    }
    actions = {
      replaceTrack: jest.fn()
    }

    store = new Vuex.Store({
      state,
      actions
    })
    const wrapper = mount(OtherResultsList, { vuetify, store, localVue })

    expect(wrapper.findAll('.v-card').length).toBe(1)
  })
  test('has 2 v-cards for 3 items', () => {
    state = {
      drawerTrack: {
        track: {
          tracks: [
            {
              name: 'test1',
              artists: [{ name: 'test1' }],
              album: { name: 'test1', images: [{}, {}, { url: 'test1' }] }
            },
            {
              name: 'test2',
              artists: [{ name: 'test2' }],
              album: { name: 'test2', images: [{}, {}, { url: 'test2' }] }
            },
            {
              name: 'test3',
              artists: [{ name: 'test3' }],
              album: { name: 'test3', images: [{}, {}, { url: 'test3' }] }
            }
          ],
          selectedTrackIndex: 0
        },
        trackIndex: 0
      }
    }
    actions = {
      replaceTrack: jest.fn()
    }

    store = new Vuex.Store({
      state,
      actions
    })
    const wrapper = mount(OtherResultsList, { vuetify, store, localVue })

    expect(wrapper.findAll('.v-card').length).toBe(2)
  })
  test('has no v-card of trackIndex v-cards for 3 items', () => {
    state = {
      drawerTrack: {
        track: {
          tracks: [
            {
              name: 'test1',
              artists: [{ name: 'test1' }],
              album: { name: 'test1', images: [{}, {}, { url: 'test1' }] }
            },
            {
              name: 'test2',
              artists: [{ name: 'test2' }],
              album: { name: 'test2', images: [{}, {}, { url: 'test2' }] }
            },
            {
              name: 'test3',
              artists: [{ name: 'test3' }],
              album: { name: 'test3', images: [{}, {}, { url: 'test3' }] }
            }
          ],
          selectedTrackIndex: 0
        },
        trackIndex: 0
      }
    }
    actions = {
      replaceTrack: jest.fn()
    }

    store = new Vuex.Store({
      state,
      actions
    })
    const wrapper = mount(OtherResultsList, { vuetify, store, localVue })

    expect(wrapper.findAll('.v-list-item__title').at(1)).not.toContain('test2')
  })
})
