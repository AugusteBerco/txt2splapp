import { mutations } from '../../store/index'

const {
  setSearchText,
  resetTracks,
  addTrackResult,
  setSearchingTracks,
  removeTrack,
  replaceTrack,
  resetState
} = mutations
describe('mutations for tracks', () => {
  test('setSearchText', () => {
    const state = { searchText: '' }
    setSearchText(state, { text: 'test' })
    expect(state.searchText).toBe('test')
  })

  test('resetTracks', () => {
    const state = {
      songs: [{ t: 't' }]
    }
    resetTracks(state)
    expect(state.songs).toEqual([])
  })

  test('addTrackResult with empty results', () => {
    const state = { songs: [] }
    const expectedTracksData = [
      {
        searched: 'test',
        found: false,
        selectedTrackIndex: 0,
        tracks: []
      }
    ]
    const payload = {
      search: 'test',
      tracks: [],
      found: false,
      selectedTrackIndex: 0
    }
    addTrackResult(state, payload)
    // expect(state.tracks).toEqual(expectedTracks)
    expect(state.songs).toStrictEqual(expectedTracksData)
  })

  test('addTrackResult with one correct payload', () => {
    const state = { songs: [] }

    // With only one result
    const payload1 = {
      search: 'test',
      tracks: [
        {
          album: {
            images: [{ url: 'test' }, { url: 'test' }, { url: 'test' }],
            name: 'test'
          },
          artists: [{ name: 'test' }],
          name: 'test'
        }
      ],
      found: true,
      selectedTrackIndex: 0
    }
    const expectedTracks1 = [payload1.items]
    const expectedTracksData1 = [
      {
        searched: 'test',
        found: true,
        selectedTrackIndex: 0,
        tracks: [
          {
            album: {
              images: [{ url: 'test' }, { url: 'test' }, { url: 'test' }],
              name: 'test'
            },
            artists: [{ name: 'test' }],
            name: 'test'
          }
        ]
      }
    ]
    addTrackResult(state, payload1)
    // expect(state.tracks).toStrictEqual(expectedTracks1)
    expect(state.songs).toStrictEqual(expectedTracksData1)
  })

  test('addTrackResult with more than one result', () => {
    const state = { songs: [] }
    const tracks = [
      {
        album: {
          images: [{ url: 'test' }, { url: 'test' }, { url: 'test' }],
          name: 'test'
        },
        artists: [{ name: 'test' }],
        name: 'test'
      },
      {
        album: {
          images: [{ url: 'test2' }, { url: 'test2' }, { url: 'test2' }],
          name: 'test2'
        },
        artists: [{ name: 'test2' }],
        name: 'test2'
      }
    ]
    // With more than one result
    const payload2 = {
      search: 'test',
      tracks,
      found: true,
      selectedTrackIndex: 0
    }
    const expectedTracksData2 = [
      {
        searched: 'test',
        found: true,
        selectedTrackIndex: 0,
        tracks
      }
    ]
    addTrackResult(state, payload2)
    // expect(state.tracks).toStrictEqual(expectedTracks2)
    expect(state.songs).toStrictEqual(expectedTracksData2)
  })

  test('addTrackResult with more than one result with index not 0', () => {
    const state = { songs: [] }
    const tracks = [
      {
        album: {
          images: [{ url: 'test' }, { url: 'test' }, { url: 'test' }],
          name: 'test'
        },
        artists: [{ name: 'test' }],
        name: 'test'
      },
      {
        album: {
          images: [{ url: 'test2' }, { url: 'test2' }, { url: 'test2' }],
          name: 'test2'
        },
        artists: [{ name: 'test2' }],
        name: 'test2'
      },
      {
        album: {
          images: [{ url: 'test3' }, { url: 'test3' }, { url: 'test3' }],
          name: 'test3'
        },
        artists: [{ name: 'test3' }],
        name: 'test3'
      }
    ]
    // With more than one result
    const payload2 = {
      search: 'test',
      tracks,
      found: true,
      selectedTrackIndex: 1
    }
    const expectedTracksData2 = [
      {
        searched: 'test',
        found: true,
        selectedTrackIndex: 1,
        tracks: tracks
      }
    ]
    addTrackResult(state, payload2)
    // expect(state.tracks).toStrictEqual(expectedTracks2)
    expect(state.songs).toStrictEqual(expectedTracksData2)
  })

  test('addTrackResult with more than one result with index is last item', () => {
    const state = { songs: [] }
    const tracks = [
      {
        album: {
          images: [{ url: 'test' }, { url: 'test' }, { url: 'test' }],
          name: 'test'
        },
        artists: [{ name: 'test' }],
        name: 'test'
      },
      {
        album: {
          images: [{ url: 'test2' }, { url: 'test2' }, { url: 'test2' }],
          name: 'test2'
        },
        artists: [{ name: 'test2' }],
        name: 'test2'
      },
      {
        album: {
          images: [{ url: 'test3' }, { url: 'test3' }, { url: 'test3' }],
          name: 'test3'
        },
        artists: [{ name: 'test3' }],
        name: 'test3'
      }
    ]
    // With more than one result
    const payload2 = {
      search: 'test',
      tracks,
      found: true,
      selectedTrackIndex: 2
    }

    const expectedTracksData2 = [
      {
        searched: 'test',
        found: true,
        selectedTrackIndex: 2,
        tracks
      }
    ]
    addTrackResult(state, payload2)
    // expect(state.tracks).toStrictEqual(expectedTracks2)
    expect(state.songs).toStrictEqual(expectedTracksData2)
  })

  test('setSearchingTracks', () => {
    const state = { searchingTracks: false }
    setSearchingTracks(state, true)
    expect(state.searchingTracks).toBeTruthy()
  })

  test('removeTrack on inititial state', () => {
    const state = { tracks: [], songs: [], searchTracks: [] }
    removeTrack(state, 3)
    expect(state.tracks).toEqual([])
    expect(state.songs).toEqual([])
    expect(state.searchTracks).toEqual([])
  })
  test('removeTrack with only 1 track', () => {
    const state = {
      songs: [
        {
          searched: 'test',
          found: true,
          selectedTrackIndex: 0,
          tracks: [
            {
              album: {
                images: [{ url: 'test' }, { url: 'test' }, { url: 'test' }],
                name: 'test'
              },
              artists: [{ name: 'test' }],
              name: 'test'
            }
          ]
        }
      ]
    }
    removeTrack(state, 0)
    // expect(state.tracks).toEqual([])
    expect(state.songs).toEqual([])
  })
  test('removeTrack with only index out of bond', () => {
    const state = {
      songs: [
        {
          searched: 'test',
          found: true,
          tracks: [
            {
              album: {
                images: [{ url: 'test' }, { url: 'test' }, { url: 'test' }],
                name: 'test'
              },
              artists: [{ name: 'test' }],
              name: 'test'
            }
          ],
          selectedTrackIndex: 0
        }
      ]
    }
    removeTrack(state, 3)

    expect(state.songs).toEqual([
      {
        searched: 'test',
        found: true,
        tracks: [
          {
            album: {
              images: [{ url: 'test' }, { url: 'test' }, { url: 'test' }],
              name: 'test'
            },
            artists: [{ name: 'test' }],
            name: 'test'
          }
        ],
        selectedTrackIndex: 0
      }
    ])
  })
  test('removeTrack with more than one items', () => {
    const state = {
      songs: [
        {
          searched: 'test',
          found: true,
          selectedTrackIndex: 0,
          tracks: [
            {
              album: {
                images: [{ url: 'test' }, { url: 'test' }, { url: 'test' }],
                name: 'test'
              },
              artists: [{ name: 'test' }],
              name: 'test'
            },
            {
              album: {
                images: [{ url: 'test2' }, { url: 'test2' }, { url: 'test2' }],
                name: 'test2'
              },
              artists: [{ name: 'test2' }],
              name: 'test2'
            }
          ]
        },
        {
          searched: 'test2',
          found: true,
          selectedTrackIndex: 0,
          tracks: [
            {
              album: {
                images: [{ url: 'test' }, { url: 'test' }, { url: 'test' }],
                name: 'test'
              },
              artists: [{ name: 'test' }],
              name: 'test'
            },
            {
              album: {
                images: [{ url: 'test2' }, { url: 'test2' }, { url: 'test2' }],
                name: 'test2'
              },
              artists: [{ name: 'test2' }],
              name: 'test2'
            }
          ]
        }
      ]
    }
    removeTrack(state, 1)

    expect(state.songs).toEqual([
      {
        searched: 'test',
        found: true,
        selectedTrackIndex: 0,
        tracks: [
          {
            album: {
              images: [{ url: 'test' }, { url: 'test' }, { url: 'test' }],
              name: 'test'
            },
            artists: [{ name: 'test' }],
            name: 'test'
          },
          {
            album: {
              images: [{ url: 'test2' }, { url: 'test2' }, { url: 'test2' }],
              name: 'test2'
            },
            artists: [{ name: 'test2' }],
            name: 'test2'
          }
        ]
      }
    ])
  })

  test('replaceTrack', () => {
    const state = {
      songs: [
        {
          searched: 'test',
          found: true,
          selectedTrackIndex: 1,
          tracks: [
            {
              album: {
                images: [{ url: 'test' }, { url: 'test' }, { url: 'test' }],
                name: 'test'
              },
              artists: [{ name: 'test' }],
              name: 'test'
            },
            {
              album: {
                images: [{ url: 'test2' }, { url: 'test2' }, { url: 'test2' }],
                name: 'test2'
              },
              artists: [{ name: 'test2' }],
              name: 'test2'
            },
            {
              album: {
                images: [{ url: 'test3' }, { url: 'test3' }, { url: 'test3' }],
                name: 'test3'
              },
              artists: [{ name: 'test3' }],
              name: 'test3'
            }
          ]
        }
      ]
    }

    replaceTrack(state, { trackIndex: 0, newSelectedTrackIndex: 1 })

    expect(state.songs).toStrictEqual([
      {
        searched: 'test',
        found: true,
        selectedTrackIndex: 1,
        tracks: [
          {
            album: {
              images: [{ url: 'test' }, { url: 'test' }, { url: 'test' }],
              name: 'test'
            },
            artists: [{ name: 'test' }],
            name: 'test'
          },
          {
            album: {
              images: [{ url: 'test2' }, { url: 'test2' }, { url: 'test2' }],
              name: 'test2'
            },
            artists: [{ name: 'test2' }],
            name: 'test2'
          },
          {
            album: {
              images: [{ url: 'test3' }, { url: 'test3' }, { url: 'test3' }],
              name: 'test3'
            },
            artists: [{ name: 'test3' }],
            name: 'test3'
          }
        ]
      }
    ])
  })

  test('replaceTrack with only one other', () => {
    const state = {
      songs: [
        {
          searched: 'test',
          found: true,
          selectedTrackIndex: 1,
          tracks: [
            {
              album: {
                images: [{ url: 'test' }, { url: 'test' }, { url: 'test' }],
                name: 'test'
              },
              artists: [{ name: 'test' }],
              name: 'test'
            },
            {
              album: {
                images: [{ url: 'test2' }, { url: 'test2' }, { url: 'test2' }],
                name: 'test2'
              },
              artists: [{ name: 'test2' }],
              name: 'test2'
            }
          ]
        }
      ]
    }

    replaceTrack(state, { trackIndex: 0, newSelectedTrackIndex: 0 })

    expect(state.songs).toStrictEqual([
      {
        searched: 'test',
        found: true,
        selectedTrackIndex: 0,
        tracks: [
          {
            album: {
              images: [{ url: 'test' }, { url: 'test' }, { url: 'test' }],
              name: 'test'
            },
            artists: [{ name: 'test' }],
            name: 'test'
          },
          {
            album: {
              images: [{ url: 'test2' }, { url: 'test2' }, { url: 'test2' }],
              name: 'test2'
            },
            artists: [{ name: 'test2' }],
            name: 'test2'
          }
        ]
      }
    ])
  })
})
