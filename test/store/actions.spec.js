import sinon from 'sinon'
import { actions } from '../../store/index'
jest.mock('spotify-web-api-js')

describe('vuex actions', () => {
  test('getUserInfo', async () => {
    const state = { spotifyClientAccessToken: 'test' }
    const commit = sinon.spy()
    await actions.getUserInfo({ commit, state })
    //expect(mockGetMe).toHaveBeenCalled()
    expect(commit.args).toEqual([['setCurrentUser', { id: 'test' }]])

    await actions.getUserInfo({ commit, state })
    expect(commit.args).toEqual([
      ['setCurrentUser', { id: 'test' }],
      ['showSnack', { msg: 'An error occurred', color: 'error' }]
    ])
  })
})
