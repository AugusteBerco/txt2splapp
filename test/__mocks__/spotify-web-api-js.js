export const mockGetMe = jest
  .fn()
  .mockImplementationOnce(() => {
    return new Promise((resolve, reject) => {
      resolve({ id: 'test' })
    })
  })
  .mockImplementationOnce(() => {
    return new Promise((resolve, reject) => {
      reject('error')
    })
  })
export const mockSetAccessToken = jest.fn()

const mock = jest.fn().mockImplementation(() => {
  return {
    accessToken: null,
    getMe: mockGetMe,
    setAccessToken: mockSetAccessToken
  }
})

export default mock
